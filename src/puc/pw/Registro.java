package puc.pw;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.SecureRandom;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controller.Acessos;
import controller.Pacientes;
import model.Paciente;

/**
 * Servlet implementation class Registro
 */
public class Registro extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Registro() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int cep = 0,telefone = 0;
		PrintWriter out = response.getWriter();
		String nome = request.getParameter("nome");
		String email = request.getParameter("email");
		if(!request.getParameter("cep").isEmpty()){
			cep = Integer.valueOf(request.getParameter("cep"));
		}
		if(!request.getParameter("telefone").isEmpty()){
			telefone = Integer.valueOf(request.getParameter("telefone"));
		}
		String complemento = request.getParameter("complemento");
		System.out.println(complemento);
		String login = null;
		String senha = null;
		
		 SecureRandom random = new SecureRandom();
		 
		 if (!nome.isEmpty()) {
             String logintxt = nome;
             logintxt = logintxt.trim();
             logintxt = logintxt.substring(0, Math.min(logintxt.length(), 5));
             login = new BigInteger(130, random).toString(32);
             login = logintxt + login.substring(0, Math.min(login.length(), 4));
             //textField_1.setText(usuario);
             senha = new BigInteger(130, random).toString(32);
             senha = senha.substring(0, Math.min(senha.length(), 9));
             //textField_2.setText(senha);
         }
		 
		 Paciente paciente = new Paciente(nome, login,email, senha, cep, telefone, complemento);
		 Pacientes p = null;
		 String message="";
		 
		 if(!nome.isEmpty()){
			 try {
				 p = new Pacientes();
				 p.incluirPaciente(paciente);
				 response.sendRedirect("Login.jsp");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 }else{
			 
			 response.sendRedirect("Register.jsp");
			 }
		 
		
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
