package puc.pw;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import controller.Pacientes;
import model.Paciente;

/**
 * Servlet implementation class Alterar
 */
public class Alterar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Alterar() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		String mensagem = null;
		String nome = request.getParameter("nome");
		String email = request.getParameter("email");
		int cep = Integer.valueOf(request.getParameter("cep"));
		int telefone = Integer.valueOf(request.getParameter("telefone"));
		String complemento = request.getParameter("complemento");
		String login = request.getParameter("login");
		String senha = request.getParameter("senha");
		HttpSession session = request.getSession();
		int id = Integer.valueOf(session.getAttribute("id").toString());
		Paciente paciente = new Paciente(id,nome, login,email, senha, cep, telefone, complemento);
		Pacientes p = null;
		try {
			p = new Pacientes();
			p.alterarPaciente(paciente);
			session.setAttribute("mensagem", "Usuario alterado com sucesso");
			session.setAttribute("nome", nome);
			session.setAttribute("email", email);
			session.setAttribute("cep", cep);
			session.setAttribute("complemento", complemento);
			session.setAttribute("numero", telefone);
			session.setAttribute("login",login);
			session.setAttribute("senha", senha);
			session.setAttribute("id", id);
			response.sendRedirect("Main.jsp");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
