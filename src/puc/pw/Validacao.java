package puc.pw;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import controller.Acessos;
import controller.MeuResultSet;
import controller.Pacientes;
import model.Paciente;

//httpsession link:
//http://www.journaldev.com/1907/java-servlet-session-management-tutorial-with-examples-of-cookies-httpsession-and-url-rewriting
/**
 * Servlet implementation class Validacao
 */
@WebServlet("/Validacao")
public class Validacao extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public Validacao() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		String login = request.getParameter("login");
		String senha = request.getParameter("senha");
		
		try {
			
			Acessos acesso = new Acessos();
			Pacientes pacientes = new Pacientes();
			Paciente paciente = null;
			
			
			if(acesso.exists(login, senha)){
				HttpSession session = request.getSession();
				ResultSet pessoa = pacientes.acharPaciente(login);
				session.setAttribute("nome", pessoa.getString("Nome"));
				session.setAttribute("email", pessoa.getString("Email"));
				session.setAttribute("cep", pessoa.getInt("CEP"));
				session.setAttribute("complemento", pessoa.getString("Complemento"));
				session.setAttribute("numero", pessoa.getInt("Numero"));
				session.setAttribute("login",request.getParameter("login"));
				session.setAttribute("senha", request.getParameter("senha"));
				session.setAttribute("id", pessoa.getInt("Id"));
				session.setAttribute("mensagem", "Usuario logado com sucesso");
	            //setting session to expiry in 30 mins
	            session.setMaxInactiveInterval(30*60);
	            Cookie userName = new Cookie("user", login);
	            userName.setMaxAge(30*60);
	            response.addCookie(userName);
				response.sendRedirect("Main.jsp");
			}else
				response.sendRedirect("Register.jsp");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		out.close();
		
	}

	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
