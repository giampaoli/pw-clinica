package controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import model.Acesso;

public class Acessos extends Conectar{

	public final static String Acessos = "acesso";
	protected final static String LOGIN = "login";
	protected final static String SENHA = "senha";
	protected final static String ID = "id";
	
	public final static String SEM_AUTORIZACAO = "FUNCIONARIO NAO CADASTRADO";

	public Acessos() throws Exception{
		if(bd == null){
			throw new Exception ("Acesso a BD nao fornecido");
		}
	}

	public List<Acesso> getAcessos(){

		List<Acesso> acessos =  new ArrayList<Acesso>();

		try {

			String sql = "SELECT * FROM " + Acessos;

			bd.prepareStatement(sql);

			MeuResultSet resultado = (MeuResultSet) bd.executeQuery();

			while(resultado.next()){
				Acesso acesso = new Acesso(resultado.getInt(ID),
												resultado.getString(LOGIN),
												resultado.getString(SENHA));				
				acessos.add(acesso);
			}

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "erro SQL ao checar pegar todos os pacientes \n" + e);
		}


		return acessos;
	}

	public boolean exists(String login, String senha){

		boolean exists = false;

		try {

			String sql = "SELECT * FROM " + Acessos + " WHERE " + LOGIN + " = ? and " + SENHA + " = ? ";

			bd.prepareStatement(sql);

			bd.setString(1, login);
			bd.setString(2, senha);

			MeuResultSet resultado = (MeuResultSet) bd.executeQuery();

			if(resultado.first()){
				int ativo = resultado.getInt("Ativo");
				if(ativo == 1){
					exists =  true;
				}
			}else{
				exists =  false;
			}

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "erro SQL ao checar existencia do paciente \n" + e);
		}
		return exists;
	}

}