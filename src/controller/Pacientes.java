package controller;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;


import model.Paciente;

public class Pacientes extends Conectar{

	public final static String PACIENTES = "pacientes";
	public final static String LOGIN = "login";
	public final static String SENHA = "senha";
	public final static String ID = "id";
	public final static String NOME = "nome";
	public final static String CEP = "cep";
	public final static String EMAIL = "email";	
	public final static String NUMERO = "numero";
	
	public final static String COMPLEMENTO = "complemento";
	
	
	
	public final static String SEM_AUTORIZACAO = "USUARIO_NAO_EXISTE";

	public Pacientes() throws Exception{
		if(bd == null){
			throw new Exception ("Acesso a BD nao fornecido");
		}
	}

	public List<Paciente> getAcessos(){

		List<Paciente> pacientes =  new ArrayList<Paciente>();

		try {

			String sql = "SELECT * FROM " + PACIENTES;

			bd.prepareStatement(sql);

			MeuResultSet resultado = (MeuResultSet) bd.executeQuery();

			while(resultado.next()){
				Paciente paciente = new Paciente(resultado.getInt(ID),
												resultado.getString(NOME),
												resultado.getString(LOGIN),
												resultado.getString(SENHA));				
				pacientes.add(paciente);
			}

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "erro SQL ao checar pegar todos os pacientes \n" + e);
		}


		return pacientes;
	}

	public boolean exists(String login, String senha){

		boolean exists = false;

		try {

			String sql = "SELECT * FROM " + PACIENTES + " WHERE " + LOGIN + " = ? and " + SENHA + " = ? ";

			bd.prepareStatement(sql);

			bd.setString(1, login);
			bd.setString(2, senha);

			MeuResultSet resultado = (MeuResultSet) bd.executeQuery();

			if(resultado.first()){
				exists =  true;
			}else{
				exists =  false;
			}

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "erro SQL ao checar existencia do paciente \n" + e);
		}
		return exists;
	}
	public void incluirPaciente(Paciente paciente) throws Exception {
        if (paciente == null) {
            throw new Exception("Paciente nao fornecido");
        }

        try {

            String sql;
            sql = "INSERT INTO acesso (LOGIN,TIPO, SENHA, ATIVO) VALUES (?,?,?,?)";

            bd.prepareStatement(sql);

            bd.setString(1, paciente.getLogin());
            bd.setInt(2, 1);
            bd.setString(3, paciente.getSenha());
            bd.setInt(4, 1);

            bd.executeUpdate();


            String sqlId = "SELECT id FROM acesso WHERE Tipo=1 AND Login LIKE '" + paciente.getLogin()
                    + "' AND Senha LIKE '" + paciente.getSenha() + "'";

            MeuResultSet resultado = (MeuResultSet) bd.executeQuery(sqlId);
            resultado.next();
            final int idAcesso = resultado.getInt(ID);

            sql = "INSERT INTO " + PACIENTES + " (ID,NOME, EMAIL,CEP,NUMERO,COMPLEMENTO) VALUES (?,?,?,?,?,?)";
            bd.prepareStatement(sql);

            bd.setInt(1, idAcesso);
            bd.setString(2, paciente.getNome());
            bd.setString(3, paciente.getEmail());
            bd.setInt(4, paciente.getCep());
            bd.setInt(5, paciente.getNumero());
            bd.setString(6, paciente.getComplemento());

            bd.executeUpdate();
            bd.commit();

        } catch (SQLException erro) {
            throw new Exception(erro);

        }
    }
	
	public void excluirPaciente(Paciente paciente) throws Exception{
		if (paciente == null) {
            throw new Exception("Paciente nao fornecido");
        }
		try {

            String sql;
            sql = "UPDATE acesso SET Ativo = 0 WHERE id = ?";

            bd.prepareStatement(sql);

            bd.setInt(1, paciente.getId());

            bd.executeUpdate();
            bd.commit();
		}catch (SQLException erro) {
            throw new Exception(erro);

        }
		
	}
	
	public void alterarPaciente(Paciente paciente) throws Exception{
		
		 if (paciente == null) {
	            throw new Exception("Paciente nao fornecido");
	        }

	        try {

	            String sql;
			sql = "UPDATE acesso SET login = ?, senha = ? WHERE id = ?";

	            bd.prepareStatement(sql);

	            bd.setString(1, paciente.getLogin());
	            bd.setString(2, paciente.getSenha());
	            bd.setInt(3, paciente.getId());

	            bd.executeUpdate();


			String sqlpaciente = "UPDATE pacientes SET Nome = ?, Email = ?, CEP = ?, Numero = ? , Complemento = ? WHERE id = ?";

	            bd.prepareStatement(sqlpaciente);
	            
	            bd.setString(1, paciente.getNome());
	            bd.setString(2, paciente.getEmail());
	            bd.setInt(3, paciente.getCep());
	            bd.setInt(4, paciente.getNumero());
	            bd.setString(5, paciente.getComplemento());
	            bd.setInt(6, paciente.getId());

	            bd.executeUpdate();
	            bd.commit();

	        } catch (SQLException erro) {
	            throw new Exception(erro);

	        }

	}
	
	
	public ResultSet acharPaciente(String login) throws SQLException{
		
		
		String sqlId = "SELECT id FROM acesso WHERE Tipo=1 AND Login LIKE '" + login + "'";

		ResultSet resultado = (ResultSet) bd.executeQuery(sqlId);
		resultado.next();
		final int idAcesso = resultado.getInt(ID);
		
		String sql = "SELECT * FROM pacientes WHERE id=" + idAcesso + "";
		
		resultado = (ResultSet) bd.executeQuery(sql);
		resultado.next();
		return resultado;

	}

}
