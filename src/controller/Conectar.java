package controller;
import java.sql.SQLException;

import controller.MeuPreparedStatement;

public class Conectar {
	
	private String servidor = "com.mysql.jdbc.Driver";
	private String urlBanco = "jdbc:mysql://localhost/BancoClinica";
	private String usuario = "root";
	private String senha = "root";
	
	protected MeuPreparedStatement bd = null;
	
	public Conectar (){
		try{
			this.setBd(new MeuPreparedStatement (
					servidor,
					urlBanco,
					usuario, senha));
			
		}catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public MeuPreparedStatement getBd() {
		return bd;
	}

	public void setBd(MeuPreparedStatement bd) {
		this.bd = bd;
	}
}
