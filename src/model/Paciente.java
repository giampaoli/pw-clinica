package model;

public class Paciente extends Acesso{

	protected String nome;
	protected int cep;
	protected int numero;
	protected String complemento;
	protected String login;
	protected String email;
	
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public Paciente(int id){
		super(id);
		
	}

	public Paciente(int id, String nome, String login, String senha){
		super(id,login,senha);
		this.nome = nome;
		
	}
	
	public Paciente(String nome,String login, String email, String senha, int cep,int numero,String complemento){
		super(login,senha);
		this.nome = nome;
		this.email = email;
		this.cep = cep;
		this.numero = numero;
		this.complemento = complemento;
	}
	public Paciente(int id,String nome,String login, String email, String senha, int cep,int numero,String complemento){
		super(id,login,senha);
		this.nome = nome;
		this.email = email;
		this.cep = cep;
		this.numero = numero;
		this.complemento = complemento;
	}
	


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getCep() {
		return cep;
	}


	public void setCep(int cep) {
		this.cep = cep;
	}


	public int getNumero() {
		return numero;
	}


	public void setNumero(int numero) {
		this.numero = numero;
	}


	public String getComplemento() {
		return complemento;
	}


	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

}
