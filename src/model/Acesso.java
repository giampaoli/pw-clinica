package model;

public class Acesso {

	protected int id;
	protected String senha;
	protected String login;

    protected int ativo;


    public int getAtivo() {
        return ativo;
    }


    public void setAtivo(int ativo) {
        this.ativo = ativo;
    }

    public Acesso(int id) {
		this.id = id;
	}
    public Acesso(int id, String login, String senha) {
		this.id = id;
		this.login = login;
		this.senha = senha;
	}
	
	public Acesso(String login, String senha) {
		this.login = login;
		this.senha = senha;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}