<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Clinica</title>
</head>
<body>
<%
	//allow access only if session exists
	String login = null;
	String nome = null;
	String email = null;
	String complemento = null;
	String mensagem = null;
	int cep,numero,id = 0;
	if (session.getAttribute("login") == null) {
		response.sendRedirect("Login.jsp");
	}
	login = (String) session.getAttribute("login");
	nome = (String) session.getAttribute("nome");
	email = (String) session.getAttribute("email");
	complemento = (String) session.getAttribute("complemento");
	cep = Integer.valueOf(session.getAttribute("cep").toString());
	numero = Integer.valueOf(session.getAttribute("numero").toString());
	id = Integer.valueOf(session.getAttribute("id").toString());
	mensagem = (String) session.getAttribute("mensagem");
	String userName = null;
	String sessionID = null;
	Cookie[] cookies = request.getCookies();
	if (cookies != null) {
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals("user"))
				userName = cookie.getValue();
			if (cookie.getName().equals("JSESSIONID"))
				sessionID = cookie.getValue();
		}
	}
	
%>

<h3><%=nome %>, <%=mensagem %></h3>
<br>
ID= <%=id %><br>
login= <%=login %><br>
nome= <%=nome%><br>
email= <%=email %><br>
cep= <%=cep %><br>
Numero= <%=numero %><br>
Complemento = <%=complemento %>
<br>
<br>
<a href="Alterar.jsp">Alterar</a><br>
<a href="Consultas.jsp">Consultas</a><br><br>
<form action="Logout" method="post">
	<input type="submit" value="Logout" >
</form>

<form action="Excluir" method="post">
	<input type="submit" value="Excluir" >
</form>
</body>
</html>