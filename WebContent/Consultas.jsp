<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="controller.MeuPreparedStatement" %>
<%@ page import="controller.MeuResultSet" %>
<%@ page import="controller.Conectar" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Consultas</title>
</head>
<body>



<%
	if (session.getAttribute("login") == null) {
		response.sendRedirect("Login.jsp");
	}

	String sql = "SELECT * FROM profissionaissaude";
	Conectar teste = new Conectar();

	teste.getBd().prepareStatement(sql);
	MeuResultSet resultado = (MeuResultSet) teste.getBd().executeQuery(sql);

	%>
	
	<form class="agendar" action="Agendar" method="doGet">
		<select name="Profissional">
		<option value="" >Selecione</option>
		<% 
	
			while (resultado.next()) {
				
			Integer idProf = resultado.getInt("IdProfissao");
			String sqlprof = "SELECT * FROM profissoes where Id = "+idProf.toString();
			Integer id = resultado.getInt("Id");
			String idString = id.toString();
			
			teste.getBd().prepareStatement(sqlprof);	
			
			
			MeuResultSet resultadoProf = (MeuResultSet) teste.getBd().executeQuery(sqlprof);
			resultadoProf.next();
			
			
		%>
			<option value="<%= idString %>"><%= resultado.getString("Nome") %> - <%=resultadoProf.getString("Nome") %></option>	
		<%} %>
	
		</select>
		<input type="submit" value="Agendar" class="register-button" >
	</form>
<br><br>

<% %>
<br><br><a href="Main.jsp">Menu</a><br>
</body>
</html>