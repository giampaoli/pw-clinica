<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Alterar</title>
</head>
<body>
<%
	//allow access only if session exists
	String login = null;
	String nome = null;
	String email = null;
	String complemento = null;
	String senha = null;
	int cep,numero,id = 0;
	if (session.getAttribute("login") == null) {
		response.sendRedirect("Login.jsp");
	}
	login = (String) session.getAttribute("login");
	senha = (String) session.getAttribute("senha");
	nome = (String) session.getAttribute("nome");
	email = (String) session.getAttribute("email");
	complemento = (String) session.getAttribute("complemento");
	cep = Integer.valueOf(session.getAttribute("cep").toString());
	numero = Integer.valueOf(session.getAttribute("numero").toString());
	id = Integer.valueOf(session.getAttribute("id").toString());
%>

<form class="alterar" action="Alterar" method="doGet">
  	Nome: <input type="Text" class="register-input" placeholder="Nome" name="nome" id="nome" value="<%=nome %>">
  	<br>
    Email: <input type="email" class="register-input" placeholder="Email" value="<%=email%>" name="email" id="email">
    <br>
    CEP: <input type="text" class="register-input" placeholder="CEP" value="<%=cep%>" name="cep" id="cep">
    <br>
    Telefone: <input type="text" class="register-input" placeholder="Telefone" value="<%=numero %>" name="telefone" id="telefone">
    <br>    
    Complemento: <input type="text" class="register-input" placeholder="Complemento" value="<%=complemento %>" name="complemento" id="complemento">
    <br>
    Login: <input type="text" class="register-input" placeholder="Login" value="<%=login%>" name="login" id="login">
	<br>
	Senha: <input type="password" class="register-input" placeholder="Senha" value="<%=senha%>" name="senha" id="senha">   
    <br>
    <input type="submit" value="Alterar" class="register-button">
  </form>
</body>
</html>