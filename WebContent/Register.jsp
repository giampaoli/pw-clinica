<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Registro</title>
  <link rel="stylesheet" href="CSS/register.css">
  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
  <h1 class="register-title">Registro - Paciente</h1>
  <form class="register" action="Registro" method="doGet">
  	<input type="Text" class="register-input" placeholder="Nome" name="nome" id="nome">
    <input type="email" class="register-input" placeholder="Email" name="email" id="email">
    <br>
    <input type="text" class="register-input" placeholder="CEP" name="cep" id="cep">
    <input type="text" class="register-input" placeholder="Telefone" name="telefone" id="telefone">
    <input type="text" class="register-input" placeholder="Complemento" name="complemento" id="complemento">
    
   
    
    <input type="submit" value="Registrar" class="register-button">
  </form>
</body>
</html>
